from django.db import models

class Cliente(models.Model):
    nome = models.CharField(max_length=50, null=False, blank=False)
    CPF = models.IntegerField(max_length=11, unique=True)
    email = models.EmailField(unique=True)
    aniversario = models.CharField(max_length=12)

    def __str__(self):
        return self.nome